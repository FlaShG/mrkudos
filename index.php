<?
  require_once('mysql_connect.inc');
  require_once('initialize_keys.inc');
  require_once('parse_plaintext_request.inc');

  require_once('handle_request.inc');

  function debug_write($s) {
    $filename = 'debug.txt';
    $handle = fopen($filename, 'w');
    fwrite($handle, $s);
    fclose($handle);
  }

  if (isset($response)) {
    $response->channel = $request['event']['channel'];
    require_once('send_response.inc');
    //debug_write(var_export($response, true));
  } else {
    //debug_write(var_export($request, true));
  }
?>
