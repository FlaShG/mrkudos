<?
  function update_score($userid, $inc) {
    global $pdo;

    $statement = $pdo->prepare('SELECT score FROM user_scores WHERE userid = ?');
    $statement->execute(array($userid));
    if ($result = $statement->fetch()) {
      $score = $result['score'] + $inc;
      $statement = $pdo->prepare('UPDATE user_scores SET score = :score WHERE userid = :userid');
    } else {
      $score = $inc;
      $statement = $pdo->prepare('INSERT INTO user_scores (userid, score) VALUES (:userid, :score)');
    }
    $success = $statement->execute(array(':userid' => $userid, ':score' => $score));
    if ($success) {
      return $score;
    }
    return false;
  }

  $pattern = '/<@([A-Z0-9]+)> ?(\+\+|--)/U';

  $point_receivers = array();
  $positive = false;
  $negative = false;
  $tried_giving_points_to_self = false;
  $tried_giving_multiple_points = false;

  $matches = array();
  if (preg_match_all($pattern, $request['event']['text'], $matches, PREG_SET_ORDER) > 0) {
    foreach ($matches as $match) {
      $user = $match[1];
      if ($match[2] == '++') {
        $inc = 1;
        $positive = true;
      } else {
        $inc = -1;
        $negative = true;
      }

      if ($user != $request['event']['user']) {
        if (!isset($point_receivers[$user])) {
          $point_receivers[$user] = update_score($user, $inc);
        } else {
          $tried_giving_multiple_points = true;
        }
      } else {
        $tried_giving_points_to_self = true;
      }
    }
  }

  if (count($point_receivers) > 0) {
    $response = new stdClass();

    if ($positive && !$negative) {
      $response_shouts = file('responses/positive.txt');
    } elseif ($negative && !$positive) {
      $response_shouts = file('responses/negative.txt');
    } else {
      $response_shouts = file('responses/mixed.txt');
    }
    $response->text = trim($response_shouts[array_rand($response_shouts)]);

    if ($tried_giving_multiple_points) {
      $response->text .= "\n...only one point at a time though.";
    }
    if ($tried_giving_points_to_self) {
      $response->text .= "\n...no self-pointerizing though.";
    }

    $listing = '';
    foreach ($point_receivers as $receiver => $score) {
      $receiver = '<@'.$receiver.'>';

      if ($listing != '') {
        $listing .= "\n";
      }
      if ($score !== false) {
        $listing .= $receiver.' now has '.$score.' points.';
      } else {
        $listing .= $receiver.'... I\'m sorry. Something\'s wrong.';
      }
    }
    $attachment = new stdClass();
    $attachment->text = $listing;
    $response->attachments = array($attachment);
  } else {
    if ($tried_giving_points_to_self) {
      $response = new stdClass();
      $response->text .= "No self-pointerizing.";
    }
  }
?>
