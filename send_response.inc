<?
  $json = json_encode((array)$response);

  $curl = curl_init('https://slack.com/api/chat.postMessage');
  // curl_setopt($curl, CURLOPT_POST, true);
  curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Authorization: Bearer '.$bot_oauth_token));
  $response = curl_exec($curl);
  curl_close($curl);
?>
